import os
import data_extract
import pandas as pd
import numpy as np
#import tensorflow as tf
#import tensorflow_hub as hub
import re
import scipy.spatial
import itertools
import logging
import sys
from multiprocessing import Pool
from flask import Flask, render_template, request, url_for
from werkzeug.exceptions import HTTPException
from collections import defaultdict
from functools import partial
import statistics
from pandas.io.json import json_normalize
import math
from math import isnan
import numpy as np
import datetime
from collections import defaultdict
import sys
from sklearn import preprocessing
import statistics

os.environ['TF_CPP_MIN_LOG_LEVEL'] = '3'


app = Flask(__name__)
app.config["SECRET_KEY"] = 'super secret key'
app.config["PROPAGATE_EXCEPTIONS"] = "DEBUG"

logging.basicConfig(
    level=logging.INFO,
    format="%(asctime)s [%(threadName)-12.12s] [%(levelname)-5.5s]  %(message)s",
    handlers=[
        logging.FileHandler("{0}/{1}.log".format('./', 'myLogFile')),
        logging.StreamHandler()
    ])
logger = logging.getLogger()
logger.info("starting application")


def transform_es_values(df_for_impact):
    ######################################################
    ###### IMPORTANT: START TO ARTICLE IMPACT SCORE ######
    ######################################################
    print("getting log values...")
    CAPPED_MV = ['yahoo.com', 'msn.com', 'linkedin.com', 'medium.com']
    CAPPED_MV_VALUE = 20 * 1000 * 1000.0
    ####################################################################
    ###### CAPPED AS CURRENTLY DONE IN ImpactScoreCalculator.java ######
    ####################################################################

    df_for_impact['site_urls_ll'] = df_for_impact['site_urls_ll'].astype(str).values
    ##################################################################################
    ###### CAP VALUES FOR ANY ARTICLES WITH PUBLICATIONS FROM THE ABOVE SOURCES ######
    ##################################################################################
    for index, article in df_for_impact.iterrows():
        for url in article['site_urls_ll'].strip('[]').split(', '):
            if any(ext in url for ext in CAPPED_MV):
                ####### IF ARTICLE site_urls_ll FOUND IN CAPPED_MV THEN ASSIGN monthly_total_visits_d WITH THE MINIMUM BETWEEN THE monthly_total_visits_d VALUE OR CAPPED_MV_VALUE
                df_for_impact.loc[index, 'monthly_total_visits_d'] = np.minimum(article['monthly_total_visits_d'], CAPPED_MV_VALUE)
            else:
                df_for_impact.loc[index, 'monthly_total_visits_d'] = article['monthly_total_visits_d']

    #####################################
    ###### FILL IN ANY NULL VALUES ######
    #####################################
    df_for_impact['monthly_total_visits_d'].fillna(value=0, inplace=True)
    df_for_impact.loc[:,df_for_impact.columns.isin(['broadcast_total_viewership_l'])].fillna(value=0, inplace=True)
    df_for_impact.loc[:,df_for_impact.columns.isin(['print_circulation_l'])].fillna(value=0, inplace=True)

    ####################################################################
    ###### DIFFERENTIATE READERSHIP BY ARTICLE PUBLICATION SOURCE ######
    ########## READERSHIP SCALED DOWN AS PER OLD ALGORITHM #############
    ####################################################################
    for index, article in df_for_impact.iterrows():
        if article['mediatype'] == 'broadcast':
            df_for_impact.loc[index, 'monthly_visits']  = np.where(article['broadcast_total_viewership_l'] >= 1.0, (article['broadcast_total_viewership_l']/100000), 1000.0/100000)
        elif article['mediatype'] == 'print':
            if 'print_circulation_l' in article:
                df_for_impact.loc[index, 'monthly_visits'] = np.where(article['print_circulation_l'] >= 1.0, (article['print_circulation_l']/100000), 1000.0/100000)
            else:
                df_for_impact.loc[index, 'monthly_visits'] = 1000.0/100000
        else:
            df_for_impact.loc[index, 'monthly_visits']  = np.where(article['monthly_total_visits_d'] >= 1.0, (article['monthly_total_visits_d']/100000), 1000.0/100000)

    ###########################################
    ###### FILL IN ANY OTHER NULL VALUES ######
    ###########################################
    df_for_impact['facebook_total_count_l'].fillna(0, inplace=True)
    df_for_impact['twitter_l'].fillna(0, inplace=True)
    df_for_impact['reddit_l'].fillna(0, inplace=True)
    df_for_impact['sentiment_value'] = df_for_impact['sentiment_value'].astype(float)
    df_for_impact['sentiment_value'].fillna(0.0, inplace=True)
    df_for_impact['domain_authority'] = df_for_impact['domain_authority_d'].fillna(0, inplace=True)

    ########################################################
    ###### BOUND SENTIMENT VALUE AS PER OLD ALGORITHM ######
    ########################################################
    df_for_impact['sentiment_min'] = np.minimum(2, abs(df_for_impact['sentiment_value']))
    df_for_impact['bounded_sentiment'] = 10**df_for_impact['sentiment_min']
    return df_for_impact



def get_entity_info(df_for_impact):
    print("getting entity info...")
    json_array = []

    #########################################################
    #### RETRIEVE ENTITY INFORMATION FROM CASSANDRA JSON ####
    #########################################################

    for index, ent_keywrd in df_for_impact.iterrows():
        try:
            ent_json_df = pd.DataFrame.from_dict(json_normalize(ent_keywrd['entities_keywords']))
            ##########################################################################################
            ######## VARIABLES WE NEED FROM 'entities_keywords' ARE 'isAbout' AND 'entityType' #######
            ##########################################################################################
            ent_json_df['publish_date'] = datetime.datetime.fromtimestamp(ent_keywrd['publish_date']/1000).strftime('%Y-%m-%d')
            ent_json_df['publisher'] = ent_keywrd['publisher']
            ent_json_df['article_id'] = ent_keywrd['article_id']
            ent_json_df['article_title'] = ent_keywrd['title']
            ent_json_df['content'] = ent_keywrd['content']
            ent_json_df['monthly_visits'] = ent_keywrd['monthly_visits']
            ent_json_df['reddit'] = ent_keywrd['reddit_l']
            ent_json_df['facebook'] = ent_keywrd['facebook_total_count_l']
            ent_json_df['twitter'] = ent_keywrd['twitter_l']
            ent_json_df['sentiment_value'] = ent_keywrd['sentiment_value']
            ent_json_df['domain_authority'] = ent_keywrd['domain_authority']
            json_array.extend(ent_json_df.values.tolist())
        except AttributeError:
            continue
        except TypeError:
            continue

    df_with_entities = pd.DataFrame(json_array)
    df_with_entities.columns = ['confident', 'entityType', 'evidence', 'isAbout', 'label',
           'sentimentPolarity', 'sentimentScore', 'themes', 'entity', 'type',
           'publish_date', 'publisher', 'article_id', 'article_title', 'content',
           'monthly_visits', 'reddit', 'facebook', 'twitter', 'sentiment_value', 'domain_authority_d']

    print("got entity info...")
    df_with_entities.entityType.replace(to_replace=dict(Pattern=0, Regex=0), inplace=True)
    #### binary flag of "0" for entity types of Pattern and Regex
    #### we do not care about these entities unless later evaluation states they're valuable or it's too expensive to retrieve cassandra json data
    df_with_entities.entityType[df_with_entities.entityType != 0] = 1
    #### binary flag of "1" for entity types we care about (Company, Place, Person, Quote)
    df_with_entities['isAbout_bin'] = df_with_entities.isAbout.astype(int)
    df_with_entities['entity'] = df_with_entities['entity'].astype(str)
    df_with_entities['entity_in_title'] = df_with_entities.apply(lambda x: x.entity.lower() in x.article_title.lower(), axis=1).astype(int)
    ########### ensure entity is set to string type and check if any Semantria tagged entities show up in the article title
    df_with_entities['title_entity_matters'] = df_with_entities['entity_in_title']*df_with_entities['entityType']*df_with_entities['isAbout_bin']
    ########### if an entity is in the title and is not Pattern or Regex and isAbout=True, then the entity matters
    df_with_entities['content'] = df_with_entities['content'].astype(str)
    df_with_entities['content'] = df_with_entities['content'].map(str.lower)
    df_with_entities['entity'] = df_with_entities['entity'].str.lower()
    for index, row in df_with_entities.iterrows():
        df_with_entities.loc[index,'entity_content_count'] = row['content'].count(row['entity'])
    df_with_entities['entity_content_mentions']= df_with_entities['entity_content_count'].fillna(0)
    ########### for each Semantria tagged entity, count how many times it occurs within content

    #################################################################################
    ##### AGGREGATE SUMS FOR ENTITY COUNTS AND MERGE BACK TO ORIGINAL DATAFRAME #####
    #################################################################################
    grouped_entity_in_content = df_with_entities.groupby(['article_id'])['entity_content_mentions'].agg('sum').reset_index()
    grouped_entities = df_with_entities.groupby(['article_id'])['title_entity_matters'].agg('sum').reset_index()
    temp_df = grouped_entity_in_content.merge(df_for_impact, on='article_id')

    final_df = grouped_entities.merge(temp_df, on='article_id')

    return final_df


def calculate_new_score(final_df):
    version = "v18_bounded_sentiment"
    print("calculating new score...")
    try:
        ###############################################
        ###### IMPORTANT TO ARTICLE IMPACT SCORE ######
        ###############################################

        ENTITY_CONTENT_MENTION_WEIGHT = 10  #A Semantria entity found in the content portion of the article is important but since it is not tailored to the client, it should be weighted least
        TITLE_ENTITY_MENTION_WEIGHT = 50  #A Semantria entity found in the title portion of the article is important and more likely to be a person, place, company, or product, and therefore should be weighted heavier than content; however, without being tailored to the client, it should not be weighted more than readership or sharing
        READERSHIP_WEIGHT = 125  #Readership is differentiated between online, print, and broadcast; prominent publications should rise towards the top to depict potential readers and is therefore weighted second highest
        SOCIAL_SHARE_WEIGHT = 150  #Social sharing between Facebook, Twitter, and Reddit is weighted highest in order to 1) show organic circulation of articles, and 2) allow viral articles with low readership to rise in ranking amongst the top publishers. This should be re-calculated as social refreshes
        DOMAIN_AUTH_WEIGHT = 50  #Domain authority is important to consider as an article's ranking on search engines is indicative of its likelihood to be clicked into/read. This should refresh ~somewhere~ as the domain authority will change over time as topics become relevant/irrelevant

        ############################################################################################################
        ###### NOTE: THIS IS A BOOST TO PRINT AND BROADCAST SOCIAL SINCE NO SOCIAL IS TIED TO THESE SOURCES ########
        ###### IT IS ADVISED TO CONSIDER IMPLICATIONS OF BOOSTING UPON INGESTION VS AT A LATER POINT IN TIME #######
        ###### UPON INGESTION WILL SHOOT PRINT/BROADCAST TO TOP RANKS UNTIL SOCIAL IS REFRESHED FOR ARTICLES #######
        ############################################################################################################
        print_broad_types = ('broadcast', 'print')
        #################################################################################################
        ###### BOOST EACH SOCIAL SHARE TO ONE TO GIVE A FAIR INCREMENTAL BOOST TO PRINT/BROADCAST #######
        #################################################################################################
        final_df['facebook_total_count_l'] = np.where(final_df['mediatype'].isin(print_broad_types), 1, final_df['facebook_total_count_l'])
        final_df['reddit_l'] = np.where(final_df['mediatype'].isin(print_broad_types), 1, final_df['reddit_l'])
        final_df['twitter_l'] = np.where(final_df['mediatype'].isin(print_broad_types), 1, final_df['twitter_l'])

        SENTIMENT = final_df['bounded_sentiment']
        READERSHIP = final_df['monthly_visits']
        FACEBOOK = final_df['facebook_total_count_l']
        TWITTER = final_df['twitter_l']
        REDDIT = final_df['reddit_l']
        ENTITY_CONTENT_MENTION = final_df['entity_content_mentions']
        TITLE_ENTITY_MENTION = final_df['title_entity_matters']
        DOMAIN_AUTH = final_df['domain_authority_d']

        ##################################################################################################################################
        ###### SENTIMENT: Bounded sentiment_value (done similarly above in python as ImpactScoreCalculator.java)
        ###### READERSHIP: Defined as monthly_total_visits_d for online content (also bounded above similarly as ImpactScoreCalculator.java for certain publishers), print_circulation_l for print content, and broadcast_total_viewership_l content
        ###### FACEBOOK, TWITTER, REDDIT: facebook_total_count_l, twitter_l, reddit_l. no transformations, only print/broadcast boost
        ###### ENTITY_CONTENT_MENTION: all counts of Semantria entities (filtered out pattern and regex, and isAbout=True) summed together
        ###### TITLE_ENTITY_MENTION: sum of binary flags if a Semantria entity (filtered out pattern and regex, and isAbout=True) appears in the title
        ###### DOMAIN_AUTH: scale of 1-100 domain_authority_d value, no transformations
        ##################################################################################################################################
        new_formula =  (SENTIMENT) +\
                           (np.log10(1 + READERSHIP) * float(READERSHIP_WEIGHT)) +\
                           (np.log10(1 + FACEBOOK + TWITTER + REDDIT) * float(SOCIAL_SHARE_WEIGHT)) +\
                           (np.log10(1 + ENTITY_CONTENT_MENTION) * float(ENTITY_CONTENT_MENTION_WEIGHT)) +\
                           (np.log10(1 + TITLE_ENTITY_MENTION) * float(TITLE_ENTITY_MENTION_WEIGHT)) +\
                           (np.log10(1 + DOMAIN_AUTH) * float(DOMAIN_AUTH_WEIGHT))
    except Exception as e:
        logger.error(e)
        print(e)
        return render_template('error.html')

    final_df['testing_impact'] = new_formula
    final_df["score_bucket"] = ""

    #################################################################
    ###### HIGH/MEDIUM/LOW LEVELS AS CURRENTLY SET IN PIPELINE ######
    #################################################################
    final_df.loc[final_df['testing_impact'] > 450, 'score_bucket'] = "High"
    final_df.loc[final_df['testing_impact'] < 150, 'score_bucket'] = "Low"
    final_df.loc[final_df['score_bucket'] == "", 'score_bucket'] = "Medium"

    return final_df


@app.route("/")
def hello():
    return render_template("start.html")


@app.route('/show', methods=['GET', 'POST'])


def index():
    ####################################################################################################################
    ############ THIS PORTION IS USED SOLELY FOR THE FLASK APP AND NOT INTEGRAL TO THE ARTICLE IMPACT SCORE ############
    ####################################################################################################################
    if request.method == "POST":
        search_id = request.form['boolean']
        if search_id == '':
            logger.warn("No search ID entered")
            return render_template('start.html', error = "Please enter a search id")
        try:
            # retrieve all article data from es and cassandra for a given search
            es_data = data_extract.extract_es(id=search_id,env='prod',max_n=10000)
            full_data = data_extract.extract_cass(es_data, 'prod')
        except Exception as e:
            logger.error(e)
            return render_template('error.html')

        df = pd.DataFrame(data=full_data)
        df["score_bucket"] = ""
        df.loc[df['impact_score_d'] > 450, 'score_bucket'] = "High"
        df.loc[df['impact_score_d'] < 150, 'score_bucket'] = "Low"
        df.loc[df['score_bucket'] == "", 'score_bucket'] = "Medium"
        df_original = df[['publisher', 'title', 'impact_score_d', 'score_bucket']].sort_values(by='impact_score_d', ascending=False)
        df_original['old_order'] = df_original.reset_index().index
        es_df = transform_es_values(df)
        entity_parsed_df = get_entity_info(es_df)
        final_df = calculate_new_score(entity_parsed_df)
        final_df = final_df[['publisher', 'title', 'testing_impact', 'score_bucket']].sort_values(by='testing_impact', ascending=False)
        df_for_merge = final_df.copy()
        df_for_merge['new_order'] = df_for_merge.reset_index().index
        merged_df = pd.merge(df_original,df_for_merge,how='inner',left_on=['publisher','title'],right_on=['publisher',"title"])
        merged_df.columns = ['publisher', 'title', 'old_impact_score', 'old_score_bucket', 'old_order', 'new_impact_score', 'new_score_bucket', 'new_order']
        merged_df = merged_df[['publisher', 'title', 'old_impact_score', 'old_score_bucket', 'new_impact_score', 'new_score_bucket', 'old_order', 'new_order']]
        merged_df = merged_df.sort_values(by='new_order', ascending=True)

    if request.form['submit_button'] == "get one table":
        return render_template('new_index.html', first_header=search_id, tables=[merged_df.to_html(index=False, border=5,justify="center",classes=["table-bordered", "table-striped", "table-hover"])],\
                                            titles=[merged_df.columns.values])
    else:
        return render_template('index.html', first_header=search_id, tables=[df_original.to_html(index=False, border=5,justify="center",classes=["table-bordered", "table-striped", "table-hover"]),
                                            final_df.to_html(index=False, border=5,justify="center",classes=["table-bordered", "table-striped", "table-hover"])],\
                                            titles=[df_original.columns.values, final_df.columns.values])


@app.route("/instructions")
def instructions():
    return render_template("instructions.html")


@app.errorhandler(Exception)
def handle_exception(e):
    if isinstance(e, HTTPException):
        return e
    return render_template("error.html", error=e), 500


if __name__ == '__main__':
    app.debug = True
    app.run(port=8080, host='0.0.0.0')
