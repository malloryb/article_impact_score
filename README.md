This is a locally hosted app created to assist with adjusting weighting to determine the current improved article impact score.

# About The New Article Impact Score:
The new article impact score calculation can be found within the **app.py** file, specifically within `transform_es_values`, `get_entity_info`, and `calculate_new_score` functions.

## New Score Calculation and Variable Definitions
At a high level, the new article impact score is calculated as follows: `(SENTIMENT) + (np.log10(1 + READERSHIP) * float(READERSHIP_WEIGHT)) + (np.log10(1 + FACEBOOK + TWITTER + REDDIT) * float(SOCIAL_SHARE_WEIGHT)) + (np.log10(1 + ENTITY_CONTENT_MENTION) * float(ENTITY_CONTENT_MENTION_WEIGHT)) + (np.log10(1 + TITLE_ENTITY_MENTION) * float(TITLE_ENTITY_MENTION_WEIGHT)) + (np.log10(1 + DOMAIN_AUTH) * float(DOMAIN_AUTH_WEIGHT))`. The variables used within this calculation are defined as:

- SENTIMENT: A bounded score derived from `sentiment_value`
- READERSHIP: Defined as `monthly_total_visits_d` for online content, print_circulation_l for print content, and broadcast_total_viewership_l content
- FACEBOOK, TWITTER, REDDIT: `facebook_total_count_l`, `twitter_l`, `reddit_l`. no transformations, only print/broadcast suggested boosts (see code comments for details)
- ENTITY_CONTENT_MENTION: Summation of Semantria's `entities_keywords` found within content (filtering out Pattern and Regex entityType, and isAbout=True)
- TITLE_ENTITY_MENTION: Summation of binary flags stating if an entity from Semantria's `entities_keywords` (filtered out pattern and regex, and isAbout=True) does or does not appear in the title
- DOMAIN_AUTH: `domain_authority_d`, no transformations

## Main Differences from Current Algorithm

- "Readership" is defined differently for online, print, and broadcast content
- Domain authority, entity presence in article title, and entity presence in article content is taken into account
- The logarithmic value of each variable is weighted separately, with each weighting correlating to the variable's article impact importance
- Print and Broadcast get artificial, fair boosts in social to make up for lack of social sharing dataType

## Steps to Produce Final calculation
Upon ingestion of the article:
- Fill null values for all variables used for impact score calculation
- Cap `monthly_total_visits_d` for articles published from sites `['yahoo.com', 'msn.com', 'linkedin.com', 'medium.com']` (same as old algorithm in ImpactScoreCalculator.java)
- Assign readership separately based on online, broadcast, and print media types
- Created bounded sentiment based off the absolute value of sentiment_value  (same as old algorithm in ImpactScoreCalculator.java)
- Filter `entities_keywords` to exclude entities where `entityType` is of type `Regex` or `Pattern`, and to only include entities where `isAbout=True`
- For each entity, check if entity appears in title. If true, flag binary "1", if false, flag binary "0"
- For each entity, count how many times the entity appears in the `content` section of the article.
- Sum the entity title binary flags to store in final `title_entity_matters` variable, sum the entity content counts to store in final `entity_content_mentions` variable
- Extract each variable value and plug into the new calculation `(SENTIMENT) + (np.log10(1 + READERSHIP) * float(READERSHIP_WEIGHT)) + (np.log10(1 + FACEBOOK + TWITTER + REDDIT) * float(SOCIAL_SHARE_WEIGHT)) + (np.log10(1 + ENTITY_CONTENT_MENTION) * float(ENTITY_CONTENT_MENTION_WEIGHT)) + (np.log10(1 + TITLE_ENTITY_MENTION) * float(TITLE_ENTITY_MENTION_WEIGHT)) + (np.log10(1 + DOMAIN_AUTH) * float(DOMAIN_AUTH_WEIGHT))`

After article ingestion:
- Social refresh kick-off should kick off a recalculation of the social share piece of the calculation and recalculate the impact score
- Since print and broadcast does not get social share information, an artificial boost has been deemed desirable for impact score. This artificial boost upon ingestion would automatically shoot print and broadcast ahead of many online articles, which in the short term (prior to social refresh adjustments) may not be a wanted outcome. Injecting the social boost for print/broadcast later after ingestion would provide a more accurate ranking in relation to other online articles, but could be messy to implement in the pipeline. Engineering should discuss when in the pipeline to input this adjustment. 

## Directions to use the app:

### Clone the app to your local computer:
- Navigate to the source directory, select "Clone" from the upper right-hand side of the screen, and copy the command provided (I use https instead of ssh option in the dropdown)
- Paste that command into your terminal. Make sure you are in a folder in your terminal where you'll know to find this app.
- After the app has successfully cloned, type `cd article_impact_score/` into your terminal

### Run the app
- If you don't have Docker on your machine, download Docker for Mac at https://docs.docker.com/docker-for-mac/install/ and follow directions to download from DockerHub
- Check your terminal to make sure you're in the `article_impact_score/` directory from the previous steps. Then run `docker image build -t article_impact_score_docker .` to build the Docker image.
- Run the app by typing `docker run -p 8081:8080 -d article_impact_score_docker` into your terminal. This should install Python 3 as well as
all of the packages needed to run the app. You can check to make sure the image is built correctly and running within your
Docker for Desktop app (press `command + space bar` and search for "Docker" and click into the application). If it's
running, the icon will be green next to the "article_impact_score_docker" image
- Copy `http://localhost:8081/` and paste into your browser. This should bring up the app - now just enter in any
production-level search ID into the text box and submit for your desired output
(there are two buttons, one to display **two side by side tables (old vs. new)** and one to display **one single table with
new and old ranking for each article**)
