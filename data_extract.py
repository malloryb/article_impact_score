import sys
import urllib.request
import os
import re
from cassandra.cluster import Cluster
import json
from cassandra.concurrent import execute_concurrent_with_args
from elasticsearch import Elasticsearch
import pathos.multiprocessing as mp
import mysql.connector

PROD = "http://esf-prod-main.aws.trendkite.com:9200"
DEV = "http://es5-dev-main.aws.trendkite.com:9200"
SITE = "http://search-prod-main.aws.trendkite.com/compose/"
KEEPERS = ['tk_readership', 'site_urls_ll', 'domain_authority_d', 'print_circulation_l', 'stumbleupon_l', 'data_source_s', 'broadcast_total_viewership_l', 'site_readership_d','content', 'lang', 'reddit_l', 'themes_keywords', 'entities_keywords', 'article_id', 'title','impact_score_d', 'sentiment_value', 'publish_date', 'mediatype','pinterest_l', 'googleplusone_l','domain_authority_d', 'sentiment_polarity', 'sentiment_value','linkedin_l', 'monthly_total_visits_d', 'frequent_terms', 'wordCount','facebook_total_count_l', 'twitter_l', 'publisher', 'publisher_url','unique_monthly_visitors_d', 'data_source_s']
JS_FIELDS = ["content^1.0", "title^1.0"]

def merge_lists(l1, l2, key):
    merged = {}
    for item in l1+l2:
        try:
            if item[key] in merged:
                merged[item[key]].update(item)
            else:
                merged[item[key]] = item
        except:
            pass
    return [val for (_, val) in merged.items()]


def extract_cass(es_data, env_key):
    print("STARTING CASS EXTRACT")
    if env_key != "prod":
        env = "cass-dev.trendkite.com"
        keyspace = "tkdev"
    else:
        env = "cass.trendkite.com"
        keyspace = "tkprod"
    cluster = Cluster([env])
    session = cluster.connect(keyspace)
    statement = session.prepare("select * from bulkstorage_tka where es_doc_id = ?")
    parameters = [(t['article_id'],) for t in es_data if t != None if 'article_id' in t.keys()]
    queries = execute_concurrent_with_args(session, statement, parameters, concurrency=50)
    cluster.shutdown()

    ##extract data
    final = []
    for x in queries:
        for t in x[1]:
            try:
                vals = [x for x in t]
                record = {"article_id": t[0]}

                if vals[7]:
                    record.update({"article_id": t[0], "themes_keywords": json.loads(vals[7])})

                if vals[3]:
                    record.update({"article_id": t[0], "entities_keywords": json.loads(vals[3])})

                if vals[1]:
                    record.update({"article_id": t[0], "auto_categories": json.loads(vals[1])})

                final.append(record)
            except Exception as e:
                pass


    ##merge the two list of dicts
    final1 = merge_lists(es_data, final , 'article_id')
    print("DONE")
    return final1


def extract_es(id, env, max_n, sysargs = {}, additions=None):
    print("STARTING EXTRACT ES")
    with urllib.request.urlopen(SITE + str(id)) as f:
        mybytes = f.read()

    mystr = mybytes.decode("utf8")

    if len(mystr) > 10:
        ##replace single quote with two single quotes
        mystr = re.sub(r"'", r"''", mystr)

        env = DEV if env != "prod" else PROD

        js_query = json.loads(mystr)
        js_query['fields'] = [
            "content^1.0",
            "title^1.0"
        ]
        js_query['default_operator'] = "and"
        search_body = json.dumps({"query": { "query_string": js_query}})

        es = Elasticsearch(env, maxsize = 25)
        result = es.search(index = "news", doc_type = "TrendKiteArticle",
                           body = search_body, size = "5000",
                           scroll = '1m', request_timeout = 30)
        es_data = result['hits']['hits']

        sid = result['_scroll_id']
        scroll_size = result['hits']['total']

        if additions == None:
        #start scroll
            while (scroll_size > 0) and (len(es_data) <= max_n):
                page = es.scroll(scroll_id = sid, scroll = '1m', request_timeout = 30)
                # Update the scroll ID and size
                sid = page['_scroll_id']
                scroll_size = len(page['hits']['hits'])
                es_data += page['hits']['hits']

        es.clear_scroll(body={'scroll_id': [sid]}, ignore=(404, ))

        print("JUST BEFORE CLEANED_LIST")
        def cleaned_list(pages, fields = KEEPERS):
            try:
                keepers = fields + ['article_id'] if isinstance(fields, list) else [fields, 'article_id']
                source = pages['_source']
                if set(['themes_keywords', 'lang']).issubset(source.keys()) and (source['lang'] == 'English'):
                    if 'social_shares' in keepers:
                        source['social_shares'] = source['twitter_l'] + source['facebook_total_count_l']
                    return {k: source[k] for k in keepers if k in source.keys()}

            except:
                pass

        #run data clean in multiprocess
        p = mp.Pool(4)
        es_data = p.map(cleaned_list, es_data)


        return es_data
    else:
        print('fail')


def loop_words(search_id, word):
    with urllib.request.urlopen(SITE + str(search_id)) as f:
        mybytes = f.read()
    mybytes = mybytes.replace(b'\\" \\"', str.encode('\\"'+word+'\\"'))
    mystr = mybytes.decode("utf8")
    mystr = re.sub(r"'",r"''", mystr)

    env = PROD

    js_query = json.loads(mystr)
    js_query['fields'] = JS_FIELDS
    js_query['default_operator'] = "and"

    search_body = json.dumps({"query": { "query_string": js_query}})

    es = Elasticsearch(env, maxsize = 25)
    result = es.search(index = "news",
                       doc_type = "TrendKiteArticle",
                       body = search_body, size = "0",
                       scroll = '1m', request_timeout = 30)
    es_data = result['hits']['hits']

    sid = result['_scroll_id']
    scroll_size = result['hits']['total']
    total = result['hits']['total']
    es.clear_scroll(body={'scroll_id': [sid]}, ignore=(404, ))
    return total


def original_boolean(search_id, words):
    db = mysql.connector.connect(
         host='trendkite-prod.c36ox2kskt73.us-east-1.rds.amazonaws.com', database='trendkite', user='trendkite',
         password='trendkite')
    query = f"SELECT search FROM search WHERE id = {search_id}"
    cursor = db.cursor()
    cursor.execute(query)
    records = cursor.fetchall()
    boolean_filled = records[0][0].replace('" "', '"'+'" OR "'.join(words)+'"' )
    return boolean_filled


def bool_dedup(bool_list):
    deduped = []
    gone_through = []
    bool_list.sort(key=len)
    for x in bool_list:
      if x not in gone_through and len(x)>2:
        temp_similars = [t for t in bool_list if x in t]
        shortest_string = min(temp_similars, key=len)
        # print(temp_similars, shortest_string)
        deduped.append(shortest_string)
        gone_through.extend(temp_similars)
    return deduped
