FROM python:3.7-buster

RUN mkdir -p /usr/service/article_impact_score
WORKDIR /usr/service/article_impact_score
COPY requirements.txt .

RUN apt-get update
# RUN apt-get install no prompt
RUN apt-get install -y build-essential

RUN pip install --upgrade -r requirements.txt
COPY app.py .
COPY data_extract.py .
COPY static static
COPY templates templates

EXPOSE 8080
EXPOSE 8081

CMD ["python","app.py"]
